run: index.tsx
	bun run --watch --port 3001 index.tsx

build: index.tsx
	bun install
	bun build index.tsx >> quati.js
