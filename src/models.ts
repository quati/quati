
export type File = {
    readonly body: ReadableStream<Uint8Array>,
    readonly opts?: ResponseInit
}

export type Page = {
    readonly title: string,
    readonly body: JSX.Element,
    readonly opts?: ResponseInit,
}

export type UserData = {
    username: string;
    email: string;
    password: string;
}

export type InputError = {
    field: string;
    message: string;
}