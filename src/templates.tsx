
export function page(title: string, body: JSX.Element): JSX.Element {
    return (
        <html lang="en">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <link rel="stylesheet" href="static/styles/style.css" />
            <title>{ title }</title>
        </head>
        <body>
            { body }
        </body>
        </html>)
}
