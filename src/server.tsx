import { page } from "./templates";
import { routes } from "./router";
import { renderToReadableStream, } from "react-dom/server";

export function run() { 
    return Bun.serve({
        fetch: async (req: Request): Promise<Response> => {
            const res = await routes(req);
            if ("title" in res) {
                const { title, body, opts } = res;
                return new Response(await handleHTMX(req, title, body), opts);
            }
            const { body, opts } = res;
            return new Response(body, opts);
        }
    })
}

async function handleHTMX({ headers }: Request, title: string, body: JSX.Element): Promise<ReadableStream<string>> {
    return renderToReadableStream(
        headers.get("HX-Request")
            ? body
            : page(title, body)
    );
}

