import type { File, Page } from "./models";

import { NOT_FOUND } from "./constants";
import { homeHandler } from "./services/home";
import { signinHandler, signupHandler } from "./services/user";

const _routes: { [key: string]: (req: Request) => Promise<Page> | Page } = {
    "/": homeHandler,
    "/signup": signupHandler,
    "/signin": signinHandler,
} as const;

export async function routes(req: Request): Promise<File | Page> {
    const { pathname } = new URL(req.url);

    if (pathname.startsWith("/static")) {
        return staticHandler(pathname);
    }

    return routeHandler(req, pathname);
}


function routeHandler(req: Request, pathname: string): Promise<Page> | Page {
    if (!(pathname in _routes)) {
        return NOT_FOUND;
    }

    return _routes[pathname](req);
}


function staticHandler(pathname: string): File | Page {
    const file = Bun.file(`./src${pathname}`);
    if (!file.exists()) {
        return NOT_FOUND;
    }
    return {
        body: file.stream()
    }
}
