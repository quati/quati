import type { Page } from "../models";

import { NOT_FOUND, OPT_HTML } from "../constants";

export function homeHandler(req: Request): Page {
    switch (req.method) {
        case "GET": 
            return { 
                title: "quati", 
                body: <h1>quati</h1>, 
                opts: { headers: OPT_HTML }, 
            };
        default: 
            return NOT_FOUND;
    }
}