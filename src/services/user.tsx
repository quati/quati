import type { Page, UserData, InputError } from "../models";

import { NOT_FOUND, OPT_HTML } from "../constants";

const users: Record<string, UserData> = {};

const signupContent = (errors: InputError[]) => (
  <div className="container">
    <form className="form" action="/signup" method="POST" hx-post="/signup">
        <ul id="message">
          {errors.map((error, index) => <li key={index}>{error.message}</li>)}
        </ul>
        <h2>Sign Up</h2>
        {input("username", "Username", "text", errors, "Valid characters are [a-A], [0-9], '-', '_', and '.'")}
        
        {input("email", "Email", "email", errors)}

        {input("password", "Password", "password", errors, "The password must be between 8 and 32 characters.")}

        {input("confirm-password", "Confirm Password","password", errors)}

      <button type="submit" hx-trigger="click">Sign Up</button>
    </form>
  </div>
);

function input(id: string, name: string, type: string, errors?: InputError[], tooltipMessage?: string) {
    return (
      <div className="input-container">
        {tooltipMessage && (
            <div>
                <div className="tooltip">
                    <label htmlFor={id}>{name}:</label>
                    <span className="tooltiptext">{tooltipMessage}</span>
                </div>
                <input className={errors?.find(error => error.field === id) ? "error" : ""} type={type} id={id} name={id} required />
            </div>
        )}
        {!tooltipMessage && (
          <>
            <label htmlFor={id}>{name}:</label>
            <input className={errors?.find(error => error.field === id) ? "error" : ""} type={type} id={id} name={id} required />
          </>
        )}
      </div>
    );
}

async function processSignup(req: Request): Promise<Page> {
  const formData = await req.formData();

  const username = formData.get("username") as string;
  const email = formData.get("email") as string;
  const password = formData.get("password") as string;
  const confirmPassword = formData.get("confirm-password") as string;
  
  console.log(req.body);

  users[email] = {
    username: username || '',
    email: email || '',
    password: password || ''
  };

  const errors = [];

  if (!username || !email || !password || !confirmPassword) {
      errors.push({ field: "", message: "Some field is missing." });
  }
  
  if (password !== confirmPassword) {
      errors.push({ field: "confirm-password", message: "The passwords don't match." });
  }
  
  if (password.length < 8 || password.length > 32) {
      errors.push({ field: "password", message: "The password must be between 8 and 32 characters." });
  }
  
  if (username.length < 2 || !/^[a-zA-Z0-9_.-]+$/.test(username) || username.length > 32) {
      errors.push({ field: "username", message: "The username must be between 2 and 32 characters." });
  }
  
  if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      errors.push({ field: "email", message: "The email is invalid" });
  }
  
  if (errors.length > 0) {
      return {
          title: "Signup",
          body: signupContent(errors),
          opts: { headers: OPT_HTML, status: 400 },
      };
  }

  return {
      title: "Boards",
      body: <h1>Welcome, {username}!</h1>,
      opts: { headers: OPT_HTML },
  };
}

export async function signupHandler(req: Request): Promise<Page> {
  switch (req.method) {
      case "GET":
          return {
              title: "Signup",
              body: signupContent([]),
              opts: { headers: OPT_HTML },
          };
      case "POST":
          return await processSignup(req);
      default:
          return NOT_FOUND;
  }
}

const signinContent = (errors: InputError[]) => (
    <div className="signin-container">
      <form className="form" action="/signin" method="POST" hx-post="/signin" hx-target="#result" hx-swap="innerHTML">
          <ul id="message">
            {errors.map((error, index) => <li key={index}>{error.message}</li>)}
          </ul>
          <h2>Sign In</h2>
          {input("email", "Email", "email", errors)}
  
          {input("password", "Password", "password", errors)}
          
        <button type="submit" hx-trigger="click">Sign Up</button>
      </form>
    </div>
  );
  
async function processSignin(req: Request): Promise<Page> {
    const formData = await req.formData();
    const email = formData.get("email") as string;
    const password = formData.get("password") as string;

    const errors = [];

    if (email !== null && users.hasOwnProperty(email) && password === users[email].password) {
        return {
            title: "Sign In",
            body: <h1>Welcome to the boards!</h1>,
            opts: { headers: OPT_HTML },
        };
      } else {
          errors.push({ field: "password", message: "The email or password is invalid" });
        return {
            title: "Sign In",
            body: signinContent(errors),
            opts: { headers: OPT_HTML, status: 400 },
        };
      }
  }

export async function signinHandler(req: Request): Promise<Page> {
    switch (req.method) {
        case "GET":
            return {
                title: "Signin",
                body: signinContent([]),
                opts: { headers: OPT_HTML },
            };
        case "POST":
            return await processSignin(req);
        default:
            return NOT_FOUND;
    }
  }
  
  