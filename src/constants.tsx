import type { Page } from "./models";

export const OPT_HTML = { "Content-Type": "text/html" }

export const NOT_FOUND: Page = { 
    title: "not found", 
    body: <h1>404 - page not found</h1>, 
    opts: { 
        headers: { "Content-Type": "text/html" },
        status: 404,
    },
}

