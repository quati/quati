FROM oven/bun:latest
COPY quati.js /quati.js
COPY src/static /src/static
EXPOSE 3000
CMD [ "bun", "/quati.js" ]
